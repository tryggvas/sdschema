@extends('layouts.app')
@section('content')
  <div class="container">
      <div class="table-responsive" style="overflow-x:auto; overflow-y:hidden;">
          
      @if ($errors->any())
          <div class="alert alert-danger">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
      @endif


          
      <form action="{{ route('admin.addNewUser')}}" method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="trainingId" value="{{$training->id}}">
          <fieldset>
            <legend>Registera användare</legend>
         <br>
         <div class="table-responsive" style="overflow-x:auto; overflow-y:hidden;border-style:none;">
             <table>
                 <tr>
                     <td><label class="left" for="first_name">Förnamn</label></td>
                     <td><input type="text" id="first_name" name="first_name"></td>
                 </tr>
                 <tr>
                     <td><label class="left" for="last_name">Efternamn</label></td>
                     <td><input type="text" id="last_name" name="last_name"></td>
                 </tr>
                 <tr>
                     <td><label class="left" for="name">Användarnamn</label></td>
                     <td><input type="text" id="name" name="name"></td>
                 </tr>
                 <tr>
                     <td><label class="left" for="password">Lösenord</label></td>
                     <td><input type="text" id="password" name="password"></td>
                 </tr>
                 <tr>
                     <td colspan="2">Lösenordet skall vara minst 6 tecken och innehålla minst en versal, en gemen och en siffra</td>
                 </tr>
                 <tr>
                     <td><label class="left" for="email">E-post</label></td>
                     <td><input type="text" id="email" name="email" size="40"></td>
                 </tr>
                 <tr>
                     <td>Singel/Par</td>
                     <td><input type="radio" id="1" name="group" checked value="1"><label class="left" for="1">Singel</label>
                         <input type="radio" id="2" name="group" value="2" style="margin-left:10px;" ><label class="left" for="2">Par</label>
                     </td>
                 </tr>
                 <tr>
                     <td><label class="left" for="authority">Behörighet</label></td>
                     <td><input type="number" id="authority" name="authority" size="4" min="1" max="{{ Auth::user()->authority}}" value="1"></td>
                 </tr>
             </table>
        </div>
          <br>
          <p style="float:right;">
         <button type="submit" class="btn btn-primary" style="margin-right:10px;">Lägg till användare</button>
            <a href="{{route('admin.showMenu',['training' => $training])}}" class="btn btn-primary" role="button">
              Avbryt
            </a>
          </p>
          </fieldset>

      </form>

     </div>
 </div>
<!--</div>-->
@section('scripts')

@endsection

@endsection
