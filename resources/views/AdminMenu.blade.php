@extends('layouts.app')
@section('content')
<h1>Admin Menu</h1>

  <div class="container">
      <div class="table-responsive" style="overflow-x:auto; overflow-y:hidden;">
         <a href="{{route('admin.showComments',['training' =>$training])}}"       class="btn btn-primary" role="button">
             Ändra kommentarer
         </a>
         <a href="{{route('admin.showAddRemoveDates',['training' =>$training])}}" class="btn btn-primary" role="button">
              Lägg till/Ta bort datum
         </a>
         <a href="{{route('admin.showMembers',['training' =>$training])}}" class="btn btn-primary" role="button">
             Medlemmar
         </a>
         <a href="{{route('admin.showAddRemoveGroup')}}" class="btn btn-primary" role="button">
             Skapa/ta bort grupp
         </a>

     </div>
 </div>
<!--</div>-->
@section('scripts')

@endsection

@endsection
