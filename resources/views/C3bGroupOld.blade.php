@extends('layouts.app')
@section('content')
<h1>C3a Måndagar</h1>
  <div class="container">
      <div class="table-responsive" style="overflow-x:auto; overflow-y:hidden;">
        @if ($edit)
        <form action="{{ route('schema.update')}}" method="POST">
          {{ csrf_field() }}
          <table class="table table-bordered table-sm table-sd-schema" style="max-width:250px;">
        @else
          <a href="{{route('schema.edit')}}" class="btn btn-primary" role="button">Ändra min närvaro</a>
          
<div class="outer">
  <div class="inner">
         
          <table class="table table-bordered" style="max-width:800px;font-size:smaller">
        @endif
            <thead>
              <th class="fix text-nowrap text-center">Datum</th>
              <th class="text-nowrap text-center">Kommentar</th>
              @if ($currentUser->authority > 1 && $edit)
              <th class="text-nowrap text-center" style="padding:2px 5px 2px 5px;">Ta bort</th>
              @else
              <th class='vertical'>Ja</th>
              <th class='vertical'>Nej</th>
              <th class='vertical'>Kanske</th>
              @endif
              @foreach ($memberNames as $memberName)
                @if (($currentUser->name==$memberName || !$edit))
                <th class="text-nowrap text-center">{{$memberName}}</th>
                @endif
              @endforeach
             </thead>
             <tbody>
             @php
               $i=0;
             @endphp
             @foreach ($trainingDates as $trainingDate)
               <tr class='status'>
               <th class="fix text-nowrap" style="padding:1px 7px;">{{$trainingDate->training_date}}</td>
               @php
                  $commentName='comment_'.$trainingDate->id;
               @endphp
               @if ($currentUser->authority > 1 && $edit) 
               <td class="text-nowrap" style="padding:1px 7px;" >
                   <input type="text" maxlength=50 size=20 name="{{$commentName}}" value="{{$trainingDate->comment}}">
               </td>
               <td style="padding:2px 5px 2px 5px;"><input type="checkbox"> Bort</td>
               @else
               <td style="padding:1px 7px;min-width:15ch;max-width:20ch;" >{{$trainingDate->comment}}</td>
               @endif
               @if (!$edit)
               <td class="text-center">{{$statusSums[$i]['Y']}}</td>
               <td class="text-center">{{$statusSums[$i]['N']}}</td>
               <td class="text-center">{{$statusSums[$i++]['M']}}</td>
               @endif
              
               @foreach ( $names as $userId => $name )
                @php
                    $status= $statuses[$userId];
                    $group= $groups[$userId];
                    $statusName='-';
                    switch ($status) {
                      case 1: if ($group==1) {
                                 $statusName='Ja';
                              } else {
                                 $statusName='1';
                              }
                              break;
                      case 2: $statusName='2';
                              break;
                      case 3: $statusName='Nej';
                              break;
                      case 4: $statusName='Kanske';
                              break;
                     }
                     $radioGroupName='status_'.$userId.'_'.$trainingDate->id;
                   @endphp
                   @if (($currentUser->name==$name && $edit))
                     <td>
                     @if ($group===1)
                        <table class="table-sd-schema" style='border:none;'>
                          <tr style='border:none;'>
                            <td style='border:none;'><input type="radio" name="{{$radioGroupName}}" value="1" class="status" {{($status==1)?'checked':''}}>
                              <span id="statusSpan">Ja</span></td>
                            <td style='border:none;'><input type="radio" name="{{$radioGroupName}}" value="3" class="status" {{($status==3)?'checked':''}}>
                              <span id="statusSpan">Nej</span></td>
                          </tr><tr style='border:none;'>
                            <td colspan="2" style='border:none;min-width:120px;text-align:center;'><input type="radio" name="{{$radioGroupName}}" value="4" class="status" {{($status==4)?'checked':''}}>
                              <span id="statusSpan">Kanske</span></td>
                          </tr>
                        </table>
                     @else
                       <table class="table-sd-schema" style='border:none;'>
                         <tr style='border:none;'>
                           <td style='border:none;min-width:75px;'><input type="radio" name="{{$radioGroupName}}" value="1" class="status" {{($status==1)?'checked':''}}>
                              <span id="statusSpan">1</span></td>
                           <td style='border:none;min-width:100px;'><input type="radio" name="{{$radioGroupName}}" value="2" class="status"  {{($status==2)?'checked':''}}>
                              <span id="statusSpan">2</span></td>
                         </tr><tr style='border:none;'>
                           <td style='border:none;'><input type="radio" name="{{$radioGroupName}}" value="3" class="status" {{($status==3)?'checked':''}}>
                             <span id="statusSpan">Nej</span></td>
                           <td style='border:none;'><input type="radio" name="{{$radioGroupName}}" value="4" class="status" {{($status==4)?'checked':''}}>
                              <span id="statusSpan">Kanske</span></td>
                         </tr>
                       </table>
                     @endif
                     </td>
                     @elseif (!$edit)
                     <td class="text-center">{{$statusName}}</td>
                     @endif
               @endforeach
               </tr>
             @endforeach
              </tbody>
            </table>

            @if ($edit)
              <button type="submit" class="btn btn-primary">Spara</button>
              <button type="cancel" class="btn btn-primary">Avbryt</button>
        </form>
        @endif
    </div>
 </div>
<!--</div>-->
@endsection
