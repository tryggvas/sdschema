@extends('layouts.app')
@section('content')
<h1>{{$training->name}} Admin</h1>
@php
    $userId=$currentUser->id;
    $tableMaxWidth=520;
    $group= $currentUser->group;
    if ($group > 1) {
        $tableMaxWidth=700;
    }
@endphp
  <div class="container">
      <div class="table-responsive" style="overflow-x:auto; overflow-y:hidden;">
         <a href="{{route('schema.showAddDates',['trainingId' =>$training->id])}}" id="addButton" class="btn btn-primary" role="button">Ändra kommentar</a>
          <a href="{{route('schema.showAddDates',['trainingId' =>$training->id])}}" id="addButton" class="btn btn-primary" role="button">Lägg till/Ta bort datum</a>
          <a href="{{route('schema.showAddDates',['trainingId' =>$training->id])}}" id="addButton" class="btn btn-primary" role="button">Lägg till/Ta bort dansare</a>
          <a href="{{route('schema.showAddDates',['trainingId' =>$training->id])}}" id="addButton" class="btn btn-primary" role="button">Skapa/ta bort grupp</a>

        <form id="myForm" action="{{ route('schema.update')}}" method="POST">
          {{ csrf_field() }}
          <fieldset>
            <legend>Ändra kommentar, eller markera datum för borttag</legend>
            
        <table class="table table-bordered" style="max-width:{{$tableMaxWidth}}px;">
        
            <thead>
              <th style="vertical-align:middle;" class="text-nowrap text-center">Datum</th>
              <th class="text-nowrap text-center" style="width:290px;">Kommentar</th>
               <th class="text-nowrap text-center" style="padding:2px 5px 2px 5px;">Ta bort</th>
            </thead>
            <tbody>

        @foreach ($trainingDates as $trainingDate)
               <tr class='status'>
                  <td class="text-nowrap" style="max-width:90px !important;height:32px; padding:2px 7px;vertical-align:middle;">{{$trainingDate->training_date}}
                  </td>
            @php
                  $commentName='comment_'.$trainingDate->id;
                  $deleteName='delete_'.$trainingDate->id;
            @endphp
                  <td class="text-nowrap" style="padding:1px 7px;" >
                     <input type="text" maxlength=50 size=40 name="{{$commentName}}" value="{{$trainingDate->comment}}">
                  </td>
                  <td style="padding:2px 5px 2px 5px;" ><input type="checkbox" class="inp" name="{{$deleteName}}"  onclick="checkBoxClicked(this.name)"> Bort
                  </td>
               </tr>
        @endforeach
            </tbody>
         </table>
            <p style="float:right;">
            <button  type="submit" class="btn btn-primary"  onclick="return checkDeletes()">Spara ändringar</button>
            <a href="{{route('schema.index',['trainingId' =>$training->id])}}" class="btn btn-primary" role="button">Avbryt</a>
            </p>
             </fieldset>

        </form>
     </div>
 </div>
<!--</div>-->
@section('scripts')
<script>
   var form = document.getElementById("myForm");

// Hide Add Dates button if form is changed
   form.addEventListener("input", function () {
      const addButton = document.getElementById("addButton");
      addButton.style.display = "none";  
   });

//Hide Add Dates button if form any date is marked for removal
function checkBoxClicked(name) {
      var n= countDeletes();
      console.log(n + ' deletes');
      const addButton = document.getElementById("addButton");
      if (n>0) {
          addButton.style.display = "none";      
      } else {
          addButton.style.display = "inline-block";      
      }
   }
   
   function countDeletes() {
      var checkedCheckBoxes=  document.querySelectorAll('.inp');
      let n=0;
      for (let i = 0; i < checkedCheckBoxes.length; i++) {
         if (checkedCheckBoxes[i].checked) {
            n++;
         }
      }
      return n;
   }
   
   function checkDeletes() {
      var n= countDeletes();
      if (n> 0 ) {
         return confirm('Är du säker? Du har markerat '+n+' datum för borttag.');
      } else {
         return true;
      }
   }     
</script>
@endsection

@endsection
