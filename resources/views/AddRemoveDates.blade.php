@extends('layouts.app')
@section('content')
<h1>Lägg till/ta bort datum</h1>
 <div class="container">

     <br>
      <form action="{{ route('admin.addDates')}}" method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="trainingId" value="{{$training->id}}">
          <fieldset>
            <legend>Ange datum som skall läggas till</legend>
      Befintliga datum är {{$weekdays}} kl. {{$danceTime}}
      Senaste datum är {{$lastTrainingDate->training_date}}
      <br><br>
         <div class="table-responsive" style="overflow-x:auto; overflow-y:hidden;">
             Lägg till <input type="number" id="quantity" name="quantity" min="1" max="15" size="4" value="1"> 
            <select id="weekdays">
              <option value="0">Söndagar</option>
              <option value="1">Måndagar</option>
              <option value="2">Tisdagar</option>
              <option value="3">Onsdagar</option>
              <option value="4">Tordagar</option>
              <option value="5">Fredagar</option>
              <option value="6">Lördagar</option>
            </select>
             från och med
             <input type="date" id="startDate" name="startDate", oninput="dateIsChanged()" value="{{$nextDate}}">
        </div>
          <br>
          <p style="float:right;">
         <button type="submit" class="btn btn-primary" style="margin-right:10px;">Lägg till datum</button>
            <a href="{{route('admin.showMenu',['training' =>$training])}}" class="btn btn-primary" role="button">
              Avbryt
            </a>
          </p>
          </fieldset>

      </form>
     <br>
     <form action="{{ route('admin.removeDates')}}" id="removeForm" method="POST">
          {{ csrf_field() }}
          <input type="hidden" name="trainingId" value="{{$training->id}}">
          <fieldset>
            <legend>Markera datum som skall tas bort</legend>
            <table class="table table-bordered table-sm table-sd-schema" style="max-width:250px;">
               <thead>
                  <th class="text-nowrap text-center">Datum</th>
                  <th class="text-nowrap text-center">Kommentar</th>
                  <th class="text-nowrap text-center" style="padding:2px 5px 2px 5px;">Ta bort</th>
               </thead>
               <tbody>
         @foreach ($trainingDates as $trainingDate)
            @php
               $deleteName='delete_'.$trainingDate->id;
            @endphp
                  <tr class='status'>
                     <td class="text-nowrap" style="padding:1px 7px;">{{$trainingDate->training_date}}</td>
                     <td class="text-nowrap" style="padding:1px 7px;" >
                         {{$trainingDate->comment}}
                     </td>
                     <td style="padding:2px 5px 2px 5px;"><input type="checkbox"  class="inp" onclick="hideOrShowRemoveButton()" name="{{$deleteName}}"> Bort</td>
                  </tr>
         @endforeach
               </tbody>
            </table>
            <br>
            <p style="float:right;">
            <button type="submit" class="btn btn-primary" id="removeButton" onclick="return checkDeletes()" style="margin-right:10px;">Ta bort datum</button>
            <a href="{{route('admin.showMenu',['training' =>$training])}}" class="btn btn-primary" role="button">
              Avbryt
            </a>
          </p>

         </fieldset>

     </form>
 </div>
@section('scripts')
<script>
 
window.onload = function() {
  checkWeekday();
  hideOrShowRemoveButton();
};
function checkWeekday() {
   let element = document.getElementById("weekdays");
    element.value = "{{$weekDaysNumber}}";
}

//  If the user picks a day from the date picker, update the the value in the
//  weekdays SELECT input element to show correct the day of week
function dateIsChanged() {
   var selectedDate = document.getElementById("startDate").value;
   var dayOfWeek= new Date(selectedDate).getDay();
   console.log(selectedDate+' veckodag='+dayOfWeek);
   let element = document.getElementById("weekdays");
   element.value= dayOfWeek;
}
//Hide Remove button if  no date is marked for removal
function hideOrShowRemoveButton() {
      var n= countDeletes();
      console.log(n + ' deletes');
      const removeButton = document.getElementById("removeButton");
      if (n==0) {
          removeButton.style.display = "none";      
      } else {
          removeButton.style.display = "inline-block";      
      }
   }
   
   function countDeletes() {
      var checkBoxes=  document.querySelectorAll('.inp');
      console.log(checkBoxes.length + ' checkBoxes');
      let n=0;
      for (let i = 0; i < checkBoxes.length; i++) {
         if (checkBoxes[i].checked) {
            n++;
         }
      }
      return n;
   }
   
   function checkDeletes() {
      var n= countDeletes();
      if (n> 0 ) {
         return confirm('Är du säker? Du har markerat '+n+' datum för borttag.');
      } else {
         return true;
      }
   }     

</script>
@endsection

@endsection
