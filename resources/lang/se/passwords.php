<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ditt lösenord har blivit återställt!',
    'sent' => 'Vi har mailat en länk för att återställa ditt lösenord!',
    'throttled' => 'Vänta ett tag innan du försöker igen.',
    'token' => 'Återställningslänken är inte giltlig.', 
    'user' => " Vi hitrar inte någon registrerad användare med den e-post-adressen.",

];
