<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVMemberTrainingDateView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement($this->dropView());
        DB::statement($this->createView());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement($this->dropView());
    }

    private function createView()
    {
        return <<<SQL
            CREATE VIEW `v_member_training_date` AS select `mtd`.`user_id` AS `user_id`,`vmt`.`user_name` AS `user_name`,`mtd`.`training_date_id` AS `training_date_id`,`td`.`training_date` AS `training_date`,`mtd`.`status` AS `status`,`vmt`.`group` AS `group` from (((`member_training_date` `mtd` left join `v_member_training` `vmt` on((`vmt`.`user_id` = `mtd`.`user_id`))) left join `training_date` `td` on((`td`.`id` = `mtd`.`training_date_id`))) left join `users` `u` on((`u`.`id` = `mtd`.`user_id`))) where (`u`.`active` = 1)
        SQL;
    }

    private function dropView()
    {
        return <<<SQL
            DROP VIEW IF EXISTS `v_member_training_date`;
        SQL;
    }
}
