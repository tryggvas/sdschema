<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVMemberTrainingView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement($this->dropView());
        DB::statement($this->createView());
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement($this->dropView());
    }

    private function createView()
    {
        return <<<SQL
            CREATE VIEW `v_member_training` AS select `u`.`id` AS `user_id`,`u`.`name` AS `user_name`,`t`.`id` AS `training_id`,`t`.`name` AS `training_name`,`u`.`group` AS `group`,`u`.`email` AS `email` from ((`member_training` `mt` left join `training` `t` on((`t`.`id` = `mt`.`training_id`))) left join `users` `u` on((`u`.`id` = `mt`.`user_id`))) where (`u`.`active` = 1)
        SQL;
    }

    private function dropView()
    {
        return <<<SQL
            DROP VIEW IF EXISTS `v_member_training`;
        SQL;
    }
}
