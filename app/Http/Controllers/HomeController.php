<?php

namespace App\Http\Controllers;

use App\Models\MemberTraining;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {

   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct() {
      //$this->middleware('auth');
   }

   public function index() {
      $count = 0;
      if (Auth::check()) {
         $myMemberTrainings = MemberTraining::where('user_id', Auth::user()->id)->get();
         $count = $myMemberTrainings->count();
      }
      if ($count == 1) {
         return redirect(route('schema.index', ['trainingId' => $myMemberTrainings[0]->training_id]));
      } else {
         return view('welcome', [
             'myTrainingsCount' => $count,
         ]);
      }
   }

   /**
    * Show the application dashboard.
    *
    * @return \Illuminate\View\View
    */
//    public function index()
//    {
//      $vMemberTrainingDates= V_MemberTrainingDate::all();
//      $trainingDates= $vMemberTrainingDates->pluck('training_date')->unique()->all();
//      $memberNames= $vMemberTrainingDates->pluck('user_name')->unique()->all();
//      return view('C3bGroup', [
//            'currentUser' => Auth::user(),
//            'vMemberTrainingDates' => $vMemberTrainingDates,
//            'trainingDates' => $trainingDates,
//            'memberNames' => $memberNames
//         ]);
//        return view('C3bGroup');
//    }
}
