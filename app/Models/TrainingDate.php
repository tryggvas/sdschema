<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// This table contains alld dates for all trainings, with start-tiem and comment
class TrainingDate extends Model {

   protected $table = 'training_date';
   protected $fillable = [
       'training_date',
   ];

}
