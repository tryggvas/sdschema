<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

// This is a many-to-many relationship
// One member can attend many trainings
// On training has many members
class MemberTraining extends Model {

   protected $table = 'member_training';
   protected $fillable = [
       'user_id',
       'training_id',
   ];
   public $timestamps = false;

}
