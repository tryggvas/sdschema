<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

 class V_MemberTraining extends Model{
      protected $table = 'v_member_training';
      public $timestamps = false;
 }