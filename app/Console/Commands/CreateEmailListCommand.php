<?php

namespace App\Console\Commands;

use App\Actions\CreateEmailListAction;
use Illuminate\Console\Command;

class CreateEmailListCommand extends Command {

   /**
    * The name and signature of the console command.
    *
    * @var string
    */
   protected $signature = 'sdSchema:emails {trainingId}';

   /**
    * The console command description.
    *
    * @var string
    */
   protected $description = 'Creates a list of email adresses for a given training';

   /**
    * Create a new command instance.
    *
    * @return void
    */
   public function __construct() {
      parent::__construct();
   }

   /**
    * Execute the console command.
    *
    * @return int
    */
   public function handle() {
      $trainingId = $this->argument('trainingId');
      $createEmailListAction = new CreateEmailListAction();
      $emails= $createEmailListAction->execute($trainingId);
      foreach ($emails as $email) {
         printf("%s\n",$email);
      }
   }

}
