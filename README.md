# Purpose
The purpose of SdSchema is to let members of a course enter an attendance status for each course date.
The application is developed for squaredance courses or trainings where it is good to know how many
membes will attend each date (if less than eight, the training might be cancelled).

# Application
SdSchema is a PHP web application using the Laravel framework, version 7.30.1

## PHP code
Most code can be found in the App/Http/Controllers/SchemaController.php program, the  App/Models folder, and the resources/views folder.
The file routes/web.php is also important

## Database
A set of (Laravel) migration files to generate the database is found in tests/database/migrations. These are not tested.
An ER-schema can be found in the database/ER_Diagram.png file

## License

The SdSchema application is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
