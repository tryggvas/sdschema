<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'HomeController@index')->name('welcome');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

//////////////////////////////////////////////////////
// All admin groups should use the isAdmin middleware
// ensuring that the user has admin privilege
//////////////////////////////////////////////////////
Route::middleware('isAdmin')->name('admin.')->group(function () {

// Add dates
   Route::name('addDates')->post('/admin.addDates', 'SchemaController@addDates');

// Add existing users to a training
   Route::name('addMember')->post('/admin/addMember', 'SchemaController@addMember');

// Add new user to the Users table
   Route::name('addNewUser')->post('/admin.addNewUser', 'SchemaController@addNewUser');

// Remove dates
   Route::name('removeDates')->post('/admin.removeDates', 'SchemaController@removeDates');

// Remove member from a training
   Route::name('removeMember')->post('/admin/removeMember', 'SchemaController@removeMember');

// Show add/remove dates view
   Route::name('showAddRemoveDates')->get('/admin/AddRemoveDates/{training}', 'SchemaController@showViewAddRemoveDates');

// Show add/remove group view
   Route::name('showAddRemoveGroup')->get('/admin/AddRemoveGroup', 'SchemaController@showViewAddRemoveGroup');
   
// Show view AdminComments
   Route::name('showComments')->get('/admin/comments/{training}', 'SchemaController@showViewAdminComments');

// Show add/remove members view
   Route::name('showMembers')->get('/admin/members/{training}', 'SchemaController@showViewMembers');

// Show admin menu
   Route::name('showMenu')->get('/admin/menu/{training}', 'SchemaController@showAdminMenu');

// Show register new user form
   Route::name('showRegisterUser')->get('/admin/showRegisterUser/{training}', 'SchemaController@showRegisterUser');

// Update comments
   Route::name('updateComments')->post('/schema.updateComments', 'SchemaController@updateComments');

});



/////////////////////////////////////////////////////////////////
// All routes in the schema groups should use the auth middleware
// ensuring that the user is logged in
/////////////////////////////////////////////////////////////////
Route::middleware('auth')->name('schema.')->group(function () {

   // Show the schema
   Route::name('index')->get('/schema.index/{trainingId?}', 'SchemaController@index');

   //Show edit view for one user for  attendance update
   Route::name('showEdit')->get('/schema/edit/{training}', 'SchemaController@showViewEdit');
   
   // Update attendance (for one user)
   Route::name('updateAttendance')->post('/schema.updateAttendance', 'SchemaController@updateAttendance');

});
